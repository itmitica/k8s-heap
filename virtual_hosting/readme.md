# Virtual hosting


## wikipedia wiki virtual hosting

[wikipedia wiki virtual hosting](https://en.wikipedia.org/wiki/Virtual_hosting)

### Name based

Name-based virtual hosts use multiple host names for the same IP address.

A technical prerequisite needed for name-based virtual hosts is a web browser with HTTP/1.1 support (commonplace today) to include the target hostname in the request. This allows a server hosting multiple sites behind one IP address to deliver the correct site's content. More specifically it means setting the Host HTTP header, which is mandatory in HTTP/1.1.[2]

### IP based

When IP-based virtual hosting is used, each site (either a DNS host name or a group of DNS host names that act the same) points to a unique IP address. The webserver is configured with multiple physical network interfaces, virtual network interfaces on the same physical interface or multiple IP addresses on one interface. The web server can either open separate listening sockets for each IP address, or it can listen on all interfaces with a single socket and obtain the IP address the TCP connection was received on after accepting the connections. Either way, it can use the IP address to determine which website to serve. The client is not involved in this process and therefore (unlike with name-based virtual hosting) there are no compatibility issues.

The downside of this approach is the server needs a different IP address for every web site. This increases administrative overhead (both assigning addresses to servers and justifying the use of those addresses to internet registries) and contributes to IPv4 address exhaustion.

## tecmint apache virtual hosting

[tecmint apache virtual hosting](https://www.tecmint.com/apache-ip-based-and-name-based-virtual-hosting/)

![tecmint apache virtual hosting](tecmint_apache_virtual_hosting.png "tecmint apache virtual hosting")

## Name Based Virtual Hosting

With the name based virtual hosting you can host several domains/websites on a single machine with a single IP. All domains on that server will be sharing a single IP. It’s easier to configure than IP based virtual hosting, you only need to configure DNS of the domain to map it with its correct IP address and then configure Apache to recognize it with the domain names.

## IP Based Virtual Hosting

With the IP based virtual hosting, you can assign a separate IP for each domain on a single server, these IP’s can be attached to the server with single NIC cards and as well as multiple NICs.
