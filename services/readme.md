## Find: AppPort

`AppPort` is `[app]port.default` or `[Dockerfile]EXPOSE`



## Find: ContainerPort

`ContainerPort` is `AppPort` or `[API.Deployment.apps]manifest.spec.template.spec.containers.ports.containerport`



## Find: PodIP

```shell
kubectl apply -f "[API.Deployment.apps]manifest"

# [API.Deployment.apps]manifest.medatada.name is pods names prefix
# Look for `PodIP` on the `IP` column
kubectl get pods -o wide -n somenamespace
```



## Access: App on Pod, inside cluster

Add firewall rules for `ContainerPort` first

Then

```shell
sudo curl PodIP:ContainerPort
```



## Access: App on Service, inside cluster


### Find: ClusterIP

```shell
# [API.Service.core]manifest.kind:      `Service`
# [API.Service.core]manifest.spec.type: `ClusterIP`
kubectl apply -f "[API.Service.core]manifest"

# [API.Service.core]manifest.metadata.name is service name
# on that line, look at the `CLUSTER-IP` column value
kubectl get services -o wide -n learning --show-labels
```

### Find: PodPort aka TargetPod


```shell
# [API.Service.core]manifest.kind:      `Service`
# [API.Service.core]manifest.spec.type: `ClusterIP`
kubectl apply -f "[API.Service.core]manifest"

sudo curl ClusterIP:TargetPort
```
