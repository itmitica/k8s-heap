# ingress

https://kubernetes.io/docs/concepts/services-networking/ingress/

Ingress is not a Service type, but it acts as the entry point for your cluster. An Ingress lets you consolidate your routing rules into a single resource, so that you can expose multiple components.


## ingress-nginx

https://kubernetes.github.io/ingress-nginx/deploy/

https://kubernetes.github.io/ingress-nginx/deploy/#bare-metal-clusters


how to proxy out:

sudo docker pull registry.k8s.io/ingress-nginx/controller:v1.7.1@sha256:7244b95ea47bddcb8267c1e625fb163fc183ef55448855e3ac52a7b260a60407


how to install:

https://metallb.universe.tf/