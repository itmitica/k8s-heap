
## What is SSL Termination?

[f5 glossary ssl-termination](https://www.f5.com/glossary/ssl-termination)

SSL termination refers to the process of decrypting encrypted traffic before passing it along to a web server.

Approximately 90% of web pages are now encrypted with the SSL (Secure Sockets Layer) protocol and its modern, more secure replacement TLS (Transport Layer Security). This is a positive development in terms of security because it prevents attackers from stealing or tampering with data exchanged between a web browser and a web or application server. But, decrypting all that encrypted traffic takes a lot of computational power—and the more encrypted pages your server needs to decrypt, the larger the burden.

SSL termination (or SSL offloading) is the process of decrypting this encrypted traffic. Instead of relying upon the web server to do this computationally intensive work, you can use SSL termination to reduce the load on your servers, speed up the process, and allow the web server to focus on its core responsibility of delivering web content.

## SSL termination proxy

![SSL termination proxy](wikipedia_wiki_TLS_termination_proxy.svg "SSL termination proxy")

[wikipedia wiki TLS termination proxy](https://en.wikipedia.org/wiki/TLS_termination_proxy)

A TLS termination proxy (or SSL termination proxy,[1] or SSL offloading[2]) is a proxy server that acts as an intermediary point between client and server applications, and is used to terminate and/or establish TLS (or DTLS) tunnels by decrypting and/or encrypting communications.

