https://kubernetes.io/docs/reference/kubernetes-api/

https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.27/

https://kubernetes.io/docs/reference/generated/kubectl/kubectl-commands


```shell

kubectl api-resources -o wide
kubectl api-versions
kubectl explain --api-version=apps/v1 Deployment --recursive
kubectl explain --api-version=apps/v1 Deployment.metadata
kubectl explain --api-version=apps/v1 Deployment.spec.template
kubectl explain --api-version=apps/v1 Deployment.spec.template.spec.containers

```