# proxy

## cloudflare

![proxies](cloudflare_glossary_proxy.svg "proxies")

[cloudflare glossary proxy](https://www.cloudflare.com/learning/cdn/glossary/reverse-proxy/)

### forward proxy

A forward proxy, often called a proxy, proxy server, or web proxy, is a server that sits in front of a group of client machines. When those computers make requests to sites and services on the Internet, the proxy server intercepts those requests and then communicates with web servers on behalf of those clients, like a middleman.

### reverse proxy

A reverse proxy is a server that sits in front of one or more web servers, intercepting requests from clients. This is different from a forward proxy, where the proxy sits in front of the clients. With a reverse proxy, when clients send requests to the origin server of a website, those requests are intercepted at the network edge by the reverse proxy server. The reverse proxy server will then send requests to and receive responses from the origin server.


## kinsta

![proxies](kinsta_blog_proxy.png "proxies")

[kinsta blog proxy](https://kinsta.com/blog/reverse-proxy/)

### forward proxy

If you want to anonymize your IP address from the websites you visit, then you can use a proxy server to send all your requests to it first. It’ll forward your requests to the DNS resolver and then download the website’s resources from its origin server.

Afterward, it’ll pass on those resources to your device. This is called a forward proxy.

### reverse proxy

A reverse proxy sits in front of a web server and receives all the requests before they reach the origin server. It works similarly to a forward proxy, except in this case it’s the web server using the proxy rather than the user or client. 

A reverse proxy is a type of proxy server that is used to protect a web server’s identity. It’s an intermediate connection point that forwards user/web browser requests to web servers, increasing performance, security, and reliability.